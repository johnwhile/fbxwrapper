# FBX MENAGER WRAPPER (WIP)

## Visual Studio 2019

- Requires installation of FBX SDK 2020.2.
The visual studio pre-compile macro automatically copy to output the correct "libfbxsdk.dll" versions.
Remember that all your C# executable require the presence of "libfbxsdk.dll" in the same folder, i suggest to automatize it with macro and "xcopy" code in the post-compiling)

- FbxWrapperTest.csproj is used to test the wrapper for managed code

- I will not convert all fbx sdk class, only those that serve to my purpose.

- I'm not a programmer but for some reason the code work, if you found compiling issue, ask me to johnwhilemail@gmail.com

