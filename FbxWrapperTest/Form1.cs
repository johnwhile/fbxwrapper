﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;

using FbxWrapper;

namespace FbxWrapperTest
{
    public partial class Form1 : Form
    {
        FBXManager FbxManager;

        FBXScene scene;
        List<FileFormat> writers;
        List<FileFormat> readers;

        public Form1()
        {
            InitializeComponent();
            ExportStripMenuItem.Enabled = false;
            scene = null;

            FbxManager = new FBXManager();

            writers = new List<FileFormat>(FbxManager.GetSupportedWriters());
            readers = new List<FileFormat>(FbxManager.GetSupportedReaders());

            ReaderBox.Items.Add("Supported Readers :");
            ReaderBox.Items.Add("");
            WriterBox.Items.Add("Supported Writers :");
            WriterBox.Items.Add("");
            foreach (FileFormat info in readers)
            {
                string str = string.Format("{0}|*.{1}", info.Description, info.Extension);
                ReaderBox.Items.Add(str);
            }
            foreach (FileFormat info in writers)
            {
                string str = string.Format("{0}|*.{1}", info.Description, info.Extension);
                WriterBox.Items.Add(str);
            }
        }

        private void ExportStripMenuItem_Click(object sender, EventArgs e)
        {
            if (scene == null) return;

            string filter = "";

            for (int i = 0; i < writers.Count; i++)
            {
                if (i > 0) filter += "|";
                filter += string.Format("{0}|*.{1}", writers[i].Description, writers[i].Extension);
            }

            // fileID is the file format index that match with writers plugin list, can be different for each Sdk version
            int fileID;
            string path = MyFileDialog.GetSaveFileName(filter, out fileID);
            if (string.IsNullOrEmpty(path)) return;

            try
            {
                //missing FbxEsporter::Create into wrapper
                //Scene.Export(scene, path, fileID - 1);
                Debug.WriteLine("OK");
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.ToString());
            }
        }

        private void OpenStripMenuItem_Click(object sender, EventArgs e)
        {
            // fileID is the file format index that match with readers plugin list can be different for each Sdk version
            int fileID;
            string filter = "";
            for (int i = 0; i < readers.Count; i++)
            {
                if (i > 0) filter += "|";
                filter += string.Format("{0}|*.{1}", readers[i].Description, readers[i].Extension);
            }

            string path = MyFileDialog.GetOpenFileName(filter, out fileID);
            if (string.IsNullOrEmpty(path)) return;

            try
            {
                scene = FBXScene.Import(FbxManager, path, -1);
                Version version = scene.FileVersion;
                Debug.WriteLine(scene.FileVersion);
                FBXNode root = scene.RootNode;
                treeView1.Nodes.Clear();
                treeView1.Nodes.Add(GetTreeNodeRecursive(root));
                treeView1.ExpandAll();
                ExportStripMenuItem.Enabled = true;
                Debug.WriteLine("OK");
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.ToString());
            }

        }

        private void BuildCustomFbx()
        {
            scene = FbxManager.CreateScene("");
            FBXNode rootnode = scene.RootNode;

            FBXNode meshnode = FbxManager.CreateNode("");
            //meshnode.Attribute = NodeAttribute.Mesh;
            rootnode.AddChild(meshnode);

            FBXMesh mesh = meshnode.Mesh;
            //mesh.SetMappingMode(LayerElementType.Normal, MappingMode.ByControlPoint);

            mesh.ControlPointsCount = 4;

            FBXVector3[] vertices = new FBXVector3[]
            {
                new FBXVector3(0,0,0),
                new FBXVector3(1,0,0),
                new FBXVector3(0,1,0),
                new FBXVector3(1,1,0)
            };
            mesh.ControlPoints = vertices;

            // one square
            //mesh.AddPolygon(new int[] { 0, 1, 3, 2 });
            // triangulated
            mesh.AddPolygon(new int[] { 0, 1, 2 });
            mesh.AddPolygon(new int[] { 2, 1, 3 });


            // i'm using my library
            FBXVector3 n1 = new FBXVector3(-1, -1, 0);
            FBXVector3 n2 = new FBXVector3(1, -1, 0);
            FBXVector3 n3 = new FBXVector3(-1, 1, 0);
            FBXVector3 n4 = new FBXVector3(1, 1, 0);
            FBXVector4 t1 = new FBXVector4(1, 0, 0, 1);

            // byControlPoint
            //FbxVector3[] normals = new FbxVector3[] { Convert(n1), Convert(n2), Convert(n3), Convert(n4) };

            // byPolygon
            FBXVector3[] normals = new FBXVector3[] { n1, n4 };

            mesh.SetNormals(normals, MappingMode.ByPolygon, ReferenceMode.Direct);

            FBXVector4[] tangents = new FBXVector4[] { t1, t1, t1, t1 };

            mesh.SetTangents(tangents, MappingMode.ByControlPoint, ReferenceMode.Direct);


            FbxManager.SetIOProperty(IOProperty.Exp_FBX_ANIMATION, false);
            FbxManager.SetIOProperty(IOProperty.Exp_FBX_EMBEDDED, false);
            FbxManager.SetIOProperty(IOProperty.Exp_FBX_MATERIAL, false);
        }

        public TreeNode GetTreeNodeRecursive(FBXNode node)
        {
            if (node == null) return null;

            NodeAttribute attribute = node.Attribute;

            TreeNode viewNode;

            if (attribute!=null)
            {
                viewNode = new TreeNode(string.Format("{0} {1}", node.Name, attribute.Type.ToString())) ;

                if (attribute.Type == AttributeType.Mesh)
                {
                    UnpackMesh(viewNode, node);
                }
            }
            else
            {
                viewNode = new TreeNode(string.Format("{0} {1}", node.Name, "null"));
            }

            for (int i = 0; i < node.GetChildCount(); i++)
            {
                FBXNode child = node.GetChild(i);
                TreeNode viewChild = GetTreeNodeRecursive(child);
                if (viewChild != null) viewNode.Nodes.Add(viewChild);
            }

            return viewNode;
        }

        public void UnpackMesh(TreeNode header, FBXNode node)
        {
            FBXMesh mesh = node.Mesh;

            Debug.WriteLine("triangulated : " + mesh.Triangulated);

            header.Nodes.Add(string.Format("Pos : {0}", node.Position.ToString()));
            header.Nodes.Add(string.Format("Scale : {0}", node.Scale.ToString()));
            header.Nodes.Add(string.Format("Rot : {0}", node.Rotation.ToString()));

            // Write vertices
            TreeNode vertexnode = new TreeNode("vertices");
            int vcount = mesh.ControlPointsCount;
            for (int i=0;i<vcount;i++)
            {
                FBXVector3 v = mesh.GetControlPointAt(i);
                vertexnode.Nodes.Add(v.ToString());
            }
            header.Nodes.Add(vertexnode);

            // Write normals
            TreeNode normalnode = new TreeNode("normals " + mesh.GetMappingMode(LayerElementType.Normal).ToString());
            FBXVector3[] normals = mesh.GetNormals();
            if (normals!=null)foreach (FBXVector3 v in normals) normalnode.Nodes.Add(v.ToString());

            // Write tangents
            //TreeNode tangentnode = new TreeNode("tangents " + mesh.GetMappingMode(LayerElementType.Tangent).ToString());
            //FbxVector4[] tangents = mesh.GetTangents();

            //if (tangents!=null) foreach (FbxVector4 v in tangents) tangentnode.Nodes.Add(v.ToString());

            // Write indices
            TreeNode polygonode = new TreeNode("indices");
            var polygons = mesh.GetPolygons();
            foreach (FbxWrapper.Polygon p in polygons) polygonode.Nodes.Add(arraytostring(p.Indices));


            TreeNode textcoordnode = new TreeNode("textcoord");
            header.Nodes.Add(normalnode);
            //header.Nodes.Add(tangentnode);
            header.Nodes.Add(polygonode);
            header.Nodes.Add(textcoordnode);
        }

        public string PolygonToString(FbxWrapper.Polygon polygon)
        {
            int[] index = polygon.Indices;
            string str = string.Join(",", index);
            return str;
        }

        private void makeCustomSceneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildCustomFbx();
            treeView1.Nodes.Clear();
            treeView1.Nodes.Add(GetTreeNodeRecursive(scene.RootNode));
            treeView1.ExpandAll();
            ExportStripMenuItem.Enabled = true;
        }

        string arraytostring(int[] a)
        {
            string str = "";
            foreach (int i in a) str += i + " ";
            return str;
        }
    }
}
