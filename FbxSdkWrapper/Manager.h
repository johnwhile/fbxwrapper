#pragma once

#include "FileFormat.h"
#include "Scene.h"
#include "IOSetting.h"
#include "NodeAttribute.h"

namespace FbxWrapper
{
	ref class FBXScene;
	ref class FBXNode;

	public ref class FBXManager
	{
	public:
		FBXManager();
		~FBXManager();

		FBXScene^ CreateScene(string^ name);
		FBXNode^ CreateNode(string^ name);

		// using List<> Generate compiling error 
		/// <summary>
		/// Plugin writer list, must be used to know the fileformatid when save file
		/// </summary> 
		array<FileFormat^>^ GetSupportedWriters();
		/// <summary>
		/// Plugin reader list, must be used to know the fileformatid when open file.
		/// </summary> 
		array<FileFormat^>^ GetSupportedReaders();
		/// <summary>
		/// Set the import-export states. By default, the export states are always set to
		/// true except for the option eEXPORT_TEXTURE_AS_EMBEDDED
		/// </summary> 
		void SetIOProperty(IOProperty prop, bool value);

	internal:
		FbxManager* m_manager;
		FbxImporter* m_importer;
		FbxExporter* m_exporter;
	};
}
