#include "stdafx.h"
#include "Scene.h"

using namespace System;
using namespace System::Runtime::InteropServices;

using namespace FbxWrapper;


FBXScene::FBXScene(FBXManager^ manager, string ^name)
{
	this->manager = manager;

	if (System::String::IsNullOrWhiteSpace(name))
		m_scene = FbxScene::Create(manager->m_manager, StringHelper::ToNative(""));
	else
		m_scene = FbxScene::Create(manager->m_manager, StringHelper::ToNative(name));

	m_rootNode = gcnew FBXNode(m_scene->GetRootNode());
}

/// <summary>
/// Export a supported fbx file defined by fileformatID (-1 for native writer... i don't known that it's)
/// found with Manager.GetSupportedWriters list
/// </summary>
void FBXScene::Export(FBXScene^ scene, string^ filename, int fileformat)
{
	if (!scene->m_scene)
		throw gcnew FbxException("nullptr");
	
	FbxExporter* exporter = manager->m_exporter;

	// NOT USE exporter->GetIOSettings() IT CRASH AFTER SECOND EXPORT ??
	if (!exporter->Initialize(StringHelper::ToNative(filename), fileformat, manager->m_manager->GetIOSettings()))
		throw gcnew FbxException("Failed to initialize the FBX exporter: {0}", gcnew string(exporter->GetStatus().GetErrorString()));

	FbxIOFileHeaderInfo *infoExporter = exporter->GetFileHeaderInfo();
	infoExporter->mCreator = "Johnwhile FbxWrapper Exporter";

	//exporter->SetFileExportVersion(FBX_FILE_VERSION_6000);

	if (!exporter->Export(scene->m_scene))
		throw gcnew FbxException("Failed to export the scene: {0}", gcnew string(exporter->GetStatus().GetErrorString()));
}

/// <summary>
/// 
/// </summary>
/// <param name="filename"></param>
/// <param name="fileformat"></param>
FBXScene ^FBXScene::Import(FBXManager^ manager, string ^filename, int fileformat)
{
	FBXScene^ scene = gcnew FBXScene(manager, filename);

	FbxManager* m_manager = manager->m_manager;

	manager->m_importer = FbxImporter::Create(m_manager, "Importer");
	FbxImporter* importer = manager->m_importer;

	if (!importer)
		throw gcnew FbxException("FBX import null");

	string^ status = gcnew string(importer->GetStatus().GetErrorString());

	//if (!importer->Initialize(StringHelper::ToNative(filename), fileformat, NULL))
	if (!importer->Initialize(StringHelper::ToNative(filename), fileformat, importer->GetIOSettings()))
		throw gcnew FbxException("Failed to initialise the FBX importer: {0}", gcnew string(importer->GetStatus().GetErrorString()));


	// Populate the FBX file format version numbers with the import file.
	int lFileMajor, lFileMinor, lFileRevision;
	importer->GetFileVersion(lFileMajor, lFileMinor, lFileRevision);
	scene->FileVersion = gcnew Version(lFileMajor, lFileMinor, 0, lFileRevision);

	if (!importer->Import(scene->m_scene))
		throw gcnew FbxException("Failed to import the scene: {0}", gcnew string(importer->GetStatus().GetErrorString()));

	importer->Destroy();

	// Needs refreshing
	scene->m_rootNode = gcnew FBXNode(scene->m_scene->GetRootNode());

	return scene;
}


FBXNode ^FBXScene::RootNode::get()
{
	return this->m_rootNode;
}