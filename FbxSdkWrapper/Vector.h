#pragma once

#include "stdafx.h"

using namespace System;

namespace FbxWrapper
{
	public value struct FBXColour
	{
	public:
		FBXColour(double r, double g, double b, double a)
		{
			R = r;
			G = g;
			B = b;
			A = a;
		}

		property double R;
		property double G;
		property double B;
		property double A;

		inline operator FbxVector4() { return FbxVector4(R, G, B, A); }
		inline operator FbxDouble4() { return FbxDouble4(R, G, B, A); }
		inline operator FbxColor() { return FbxColor(R, G, B, A); }

		virtual String ^ToString() override
		{
			return String::Format("{0}, {1}, {2}, {3}", Math::Round(R, 3), Math::Round(G, 3), Math::Round(B, 3), Math::Round(A, 3));
		}

	internal:
		FBXColour(FbxColor fbxColour)
		{
			R = fbxColour.mRed;
			G = fbxColour.mGreen;
			B = fbxColour.mBlue;
			A = fbxColour.mAlpha;
		}
	};

	public value struct FBXVector3
	{
	public:
		FBXVector3(double x, double y, double z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		property double X;
		property double Y;
		property double Z;

		inline operator FbxDouble3() { return FbxDouble3(X, Y, Z); }
		inline operator FbxVector4() { return FbxVector4(X, Y, Z, 0); }


		virtual String ^ToString() override
		{
			return String::Format("{0}, {1}, {2}", Math::Round(X, 3), Math::Round(Y, 3), Math::Round(Z, 3));
		}

		static FBXVector3 operator *(FBXVector3 self, float value)
		{
			self.X *= value;
			self.Y *= value;
			self.Z *= value;
			return self;
		}

	internal:
		FBXVector3(FbxDouble3 fbxDouble)
		{
			X = fbxDouble[0];
			Y = fbxDouble[1];
			Z = fbxDouble[2];
		}
	};

	public value struct FBXVector2
	{
		FBXVector2(double x, double y)
		{
			X = x;
			Y = y;
		}
		inline operator FbxVector2() { return FbxVector2(X, Y); }
		inline operator FbxDouble2() { return FbxDouble2(X, Y); }

		virtual String ^ToString() override
		{
			return String::Format("{0}, {1}", Math::Round(X, 3), Math::Round(Y, 3));
		}

		property double X;
		property double Y;

	internal:
		FBXVector2(FbxVector2 vector)
		{
			X = vector[0];
			Y = vector[1];
		}
	};

	public value struct FBXVector4
	{
	internal:
		FBXVector4(FbxVector4 vector)
		{
			X = vector.mData[0];
			Y = vector.mData[1];
			Z = vector.mData[2];
			W = vector.mData[3];
		}
	public:
		FBXVector4(double x, double y, double z, double w)
		{
			W = w;
			X = x;
			Y = y;
			Z = z;
		}

		inline operator FbxVector4() { return FbxVector4(X, Y, Z, W); }
		inline operator FbxDouble4() { return FbxDouble4(X, Y, Z, W); }
		

		virtual String ^ToString() override
		{
			return String::Format("{0}, {1}, {2}, {3}", Math::Round(X, 3), Math::Round(Y, 3), Math::Round(Z, 3), Math::Round(W, 3));
		}
		property double X;
		property double Y;
		property double Z;
		property double W;
	};

	public value struct FBXMatrix4
	{
	internal:
		FBXMatrix4(FbxAMatrix matrix)
		{
			m00 = matrix.mData[0].mData[0];
			m10 = matrix.mData[1].mData[0];
			m20 = matrix.mData[2].mData[0];
			m30 = matrix.mData[3].mData[0];

			m01 = matrix.mData[0].mData[1];
			m11 = matrix.mData[1].mData[1];
			m21 = matrix.mData[2].mData[1];
			m31 = matrix.mData[3].mData[1];

			m02 = matrix.mData[0].mData[2];
			m12 = matrix.mData[1].mData[2];
			m22 = matrix.mData[2].mData[2];
			m32 = matrix.mData[3].mData[2];

			m03 = matrix.mData[0].mData[3];
			m13 = matrix.mData[1].mData[3];
			m23 = matrix.mData[2].mData[3];
			m33 = matrix.mData[3].mData[3];
		}
	public :
		FBXMatrix4(double m00, double m10, double m20, double m30, double m01, double m11, double m21, double m31, double m02, double m12, double m22, double m32, double m03, double m13, double m23, double m33)
		{
			this->m00 = m00;
			this->m10 = m10;
			this->m20 = m20;
			this->m30 = m30;
			this->m01 = m01;
			this->m11 = m11;
			this->m21 = m21;
			this->m31 = m31;
			this->m02 = m02;
			this->m12 = m12;
			this->m22 = m22;
			this->m32 = m32;
			this->m03 = m03;
			this->m13 = m13;
			this->m23 = m23;
			this->m33 = m33;
		}
		inline operator FbxAMatrix()
		{
			FbxAMatrix m;
			m[0] = FbxVector4(this->m00, this->m10, this->m20, this->m30);
			m[1] = FbxVector4(this->m01, this->m11, this->m21, this->m31);
			m[2] = FbxVector4(this->m02, this->m12, this->m22, this->m32);
			m[3] = FbxVector4(this->m03, this->m13, this->m23, this->m33);
			return m;
		}

		virtual String^ ToString() override
		{
			return nullptr;
			//return String::Format("{0}\n{1}\n{2}\n{3}", row0, row1, row2, row3);
		}

		property double m00;
		property double m10;
		property double m20;
		property double m30;
		property double m01;
		property double m11;
		property double m21;
		property double m31;
		property double m02;
		property double m12;
		property double m22;
		property double m32;
		property double m03;
		property double m13;
		property double m23;
		property double m33;
	};




}
