#include "stdafx.h"
#include "Mesh.h"
#include "fbxsdk\scene\geometry\fbxlayer.h"

using namespace FbxWrapper;
using namespace System::Diagnostics;


FBXMesh::FBXMesh(FbxMesh* pmesh)
{
	m_mesh = pmesh;
}

bool FBXMesh::Triangulated::get()
{
	return m_mesh->IsTriangleMesh();
}

MappingMode FBXMesh::GetMappingMode(LayerElementType elementType)
{
	FbxLayerElement::EType etype = (FbxLayerElement::EType)elementType;

	if (auto layer = m_mesh->GetLayer(0, etype))
	{
		return (MappingMode)layer->GetLayerElementOfType(etype)->GetMappingMode();
	}
	return MappingMode::None;
}


#pragma region Vertices

int FBXMesh::ControlPointsCount::get()
{
	return m_mesh->GetControlPointsCount();
}

void FBXMesh::ControlPointsCount::set(int count)
{
	if (count != m_mesh->GetControlPointsCount())
		m_mesh->SetControlPointCount(count);
}

bool FBXMesh::SetControlPointAt(int index, double x, double y, double z)
{
	m_mesh->SetControlPointAt(FbxVector4(x, y, z, 0), index);
	return index < m_mesh->GetControlPointsCount();
}

FBXVector3 FBXMesh::GetControlPointAt(int index)
{
	return FBXVector3(m_mesh->GetControlPointAt(index));
}

array<FBXVector3> ^FBXMesh::ControlPoints::get()
{
	int count = m_mesh->GetControlPointsCount();
	auto list = gcnew array<FBXVector3>(count);
	for (int i = 0; i < count; i++)
		list[i] = FBXVector3(m_mesh->GetControlPointAt(i));
	return list;
}

void FBXMesh::ControlPoints::set(array<FBXVector3> ^controlpoints)
{
	int count = controlpoints->Length;
	for (int i = 0; i < count; i++)
	{
		FBXVector3 v = controlpoints[i];
		m_mesh->SetControlPointAt((FbxVector4)v, i);
	}
}

#pragma endregion


bool FBXMesh::SetNormals(array<FBXVector3>^ normals, MappingMode mapping, ReferenceMode referencing)
{
	// Set the normals on Layer 0.
	auto layer0 = m_mesh->GetLayer(0);
	if (layer0 == NULL)
	{
		m_mesh->CreateLayer();
		layer0 = m_mesh->GetLayer(0);
	}
	FbxLayerElementTemplate<FbxVector4> *normalLayer = FbxLayerElementNormal::Create(m_mesh, "normals");
	int size = normals->Length;
	FbxVector4 *list = new FbxVector4[size];
	for (int i = 0; i < size; i++)
	{
		FBXVector3 n = normals[i];
		list[i] = (FbxVector4)n;
	}
	SetLayerElementTemplate<FbxVector4>(m_mesh, normalLayer, list, size, mapping, referencing);
	layer0->SetLayerElementOfType(normalLayer, FbxLayerElement::EType::eNormal);
	return true;
}

bool FBXMesh::SetTangents(array<FBXVector4>^ tangents, MappingMode mapping, ReferenceMode referencing)
{
	// Set the normals on Layer 0.
	auto layer0 = m_mesh->GetLayer(0);
	if (layer0 == NULL)
	{
		m_mesh->CreateLayer();
		layer0 = m_mesh->GetLayer(0);
	}
	FbxLayerElementTemplate<FbxVector4> *tangentLayer = FbxLayerElementTangent::Create(m_mesh, "tangents");
	int size = tangents->Length;
	FbxVector4 *list = new FbxVector4[size];
	for (int i = 0; i < size; i++)
	{
		FBXVector4 n = tangents[i];
		list[i] = (FbxVector4)n;
	}
	SetLayerElementTemplate<FbxVector4>(m_mesh, tangentLayer, list, size, mapping, referencing);
	layer0->SetLayerElementOfType(tangentLayer, FbxLayerElement::EType::eTangent);
	return true;
}

bool FBXMesh::SetTextCoords(array<FBXVector2>^ textcoords, MappingMode mapping, ReferenceMode referencing)
{
	// Set the normals on Layer 0.
	auto layerN = m_mesh->GetLayer(UVLayer);
	if (layerN == NULL)
	{
		m_mesh->CreateLayer();
		layerN = m_mesh->GetLayer(UVLayer);
	}
	FbxLayerElementTemplate<FbxVector2> *textcoordLayer = FbxLayerElementUV::Create(m_mesh, "textcoord");
	int size = textcoords->Length;
	FbxVector2 *list = new FbxVector2[size];
	for (int i = 0; i < size; i++)
	{
		FBXVector2 n = textcoords[i];
		list[i] = (FbxVector2)n;
	}
	SetLayerElementTemplate<FbxVector2>(m_mesh, textcoordLayer, list, size, mapping, referencing);
	
	layerN->SetLayerElementOfType(textcoordLayer, FbxLayerElement::EType::eUV);
	
	return true;
}

bool FBXMesh::SetVertexColours(array<FBXColour>^ colours, MappingMode mapping, ReferenceMode referencing)
{
	// Set the normals on Layer 0.
	auto layer0 = m_mesh->GetLayer(0);
	if (layer0 == NULL)
	{
		m_mesh->CreateLayer();
		layer0 = m_mesh->GetLayer(0);
	}
	FbxLayerElementTemplate<FbxColor> *coloursLayer = FbxLayerElementVertexColor::Create(m_mesh, "VertexColours");
	int size = colours->Length;
	FbxColor *list = new FbxColor[size];
	for (int i = 0; i < size; i++)
	{
		FBXColour n = colours[i];
		list[i] = (FbxColor)n;
	}
	SetLayerElementTemplate<FbxColor>(m_mesh, coloursLayer, list, size, mapping, referencing);

	layer0->SetLayerElementOfType(coloursLayer, FbxLayerElement::EType::eVertexColor);

	return true;
}



array<FBXVector3> ^FBXMesh::GetNormals()
{
	auto layer0 = m_mesh->GetLayer(0);
	if (layer0 == NULL) return nullptr;

	array<FBXVector3> ^vectors = nullptr;
	int size;
	FbxVector4* plist = GetLayerElementTemplate<FbxVector4>(m_mesh, layer0->GetNormals(), size);
	if (plist)
	{
	    vectors = gcnew array<FBXVector3>(size);
		for (int i = 0; i < size; i++) vectors[i] = FBXVector3(plist[i]);
	}
	return vectors;
}

array<FBXVector4> ^FBXMesh::GetTangents()
{
	auto layer0 = m_mesh->GetLayer(0);
	if (layer0 == NULL) return nullptr;

	array<FBXVector4> ^vectors = nullptr;
	int size;
	FbxVector4* plist = GetLayerElementTemplate<FbxVector4>(m_mesh, layer0->GetTangents(), size);
	if (plist)
	{
		vectors = gcnew array<FBXVector4>(size);
		for (int i = 0; i < size; i++) vectors[i] = FBXVector4(plist[i]);
	}
	return vectors;
}

array<FBXVector2> ^FBXMesh::GetTextCoords()
{
	auto layerN = m_mesh->GetLayer(UVLayer);
	if (layerN == NULL) return nullptr;

	array<FBXVector2> ^vectors = nullptr;
	int size;
	FbxVector2* plist = GetLayerElementTemplate<FbxVector2>(m_mesh, layerN->GetUVs(), size);
	if (plist)
	{
		vectors = gcnew array<FBXVector2>(size);
		for (int i = 0; i < size; i++) vectors[i] = FBXVector2(plist[i]);
	}
	return vectors;
}

array<FBXColour> ^FBXMesh::GetVertexColours()
{
	auto layer0 = m_mesh->GetLayer(0);
	if (layer0 == NULL) return nullptr;

	array<FBXColour> ^vectors = nullptr;
	int size;
	FbxColor* plist = GetLayerElementTemplate<FbxColor>(m_mesh, layer0->GetVertexColors(), size);
	if (plist)
	{
		vectors = gcnew array<FBXColour>(size);
		for (int i = 0; i < size; i++) vectors[i] = FBXColour(plist[i]);
	}
	return vectors;
}



void FBXMesh::AddPolygon(array<int>^ indices)
{
	m_mesh->BeginPolygon(-1, -1, -1, false);	
	for each (int i in indices)
		m_mesh->AddPolygon(i);
	m_mesh->EndPolygon();
}

array<FbxWrapper::Polygon> ^FBXMesh::GetPolygons()
{
	int count = m_mesh->GetPolygonCount();
	auto list = gcnew array<Polygon>(count);

	for (int i = 0; i < count; i++)
	{
		auto poly = Polygon();

		int indexCount = m_mesh->GetPolygonSize(i);
		poly.Indices = gcnew array<int>(indexCount);

		for (int j = 0; j < indexCount; j++)
			poly.Indices[j] = m_mesh->GetPolygonVertex(i, j);

		list[i] = poly;
	}

	return list;
}

/*


array<Colour> ^Mesh::VertexColours::get()
{
	auto colours = m_mesh->GetLayer(0)->GetVertexColors();
	int count = colours == nullptr ? 0 : colours->GetDirectArray().GetCount();
	auto list = gcnew array<Colour>(count);

	for (int i = 0; i < count; i++)
		list[i] = Colour(colours->GetDirectArray().GetAt(i));

	return list;
}

array<int> ^Mesh::MaterialIDs::get()
{
	auto materials = m_mesh->GetLayer(0)->GetMaterials();
	int count = materials == nullptr ? 0 : materials->GetIndexArray().GetCount();
	auto list = gcnew array<int>(count);

	for (int i = 0; i < count; i++)
		list[i] = materials->GetIndexArray().GetAt(i);

	return list;
}
*/