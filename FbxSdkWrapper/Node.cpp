#include "stdafx.h"
#include "Node.h"

using namespace FbxWrapper;


FBXNode::FBXNode(FBXManager^ manager, string^ name) : FBXNode::FBXNode(manager, name, AttributeType::Unknown) {}

FBXNode::FBXNode(FbxNode* pnode)
{
	m_node = pnode;
}

FBXNode::FBXNode(FBXManager^ manager, string^ name, AttributeType etype)
{
	m_node = FbxNode::Create(manager->m_manager, StringHelper::ToNative(name));

	switch (etype)
	{
	case AttributeType::Mesh:
		m_node->SetNodeAttribute(FbxMesh::Create(manager->m_manager, StringHelper::ToNative(name)));
		break;
	case AttributeType::Skeleton:
		m_node->SetNodeAttribute(FbxSkeleton::Create(manager->m_manager, StringHelper::ToNative(name)));
		break;
	default:
		//throw gcnew FbxException("Attribute type {0} not implemented", etype.ToString());
		break;
	}
}


string ^FBXNode::Name::get()
{
	return gcnew string(m_node->GetName());
}

NodeAttribute ^FBXNode::Attribute::get()
{
	FbxNodeAttribute *a = m_node->GetNodeAttribute();

	if (a == nullptr)
	{
		//throw gcnew FbxException("Native node attribute is null");
		return nullptr;
	}
	return gcnew NodeAttribute(m_node->GetNodeAttribute());
}

int FBXNode::GetChildCount()
{
	return m_node->GetChildCount(false);
}

FBXNode ^FBXNode::GetChild(int index)
{
	return gcnew FBXNode(m_node->GetChild(index));
}

void FBXNode::AddChild(FBXNode ^node)
{
	m_node->AddChild(node->m_node);
}


/// <summary>
/// Get the node�s default TRS properties (position)
/// </summary>
FBXVector3 FBXNode::Position::get()
{
	return FBXVector3(m_node->LclTranslation.Get());
}

void FBXNode::Position::set(FBXVector3 value)
{
	m_node->LclTranslation.Set(value);
}

/// <summary>
/// Get the node�s default TRS properties (rotation)
/// </summary>
FBXVector3 FBXNode::Rotation::get()
{
	return FBXVector3(m_node->LclRotation.Get());
}

void FBXNode::Rotation::set(FBXVector3 value)
{
	m_node->LclRotation.Set(value);
}
/// <summary>
/// Get the node�s default TRS properties (scale)
/// </summary>
FBXVector3 FBXNode::Scale::get()
{
	return FBXVector3(m_node->LclScaling.Get());
}
void FBXNode::Scale::set(FBXVector3 value)
{
	m_node->LclScaling.Set(value);
}

FbxAMatrix FBXNode::getGeometricTransform()
{
	FbxAMatrix matrixGeo;
	matrixGeo.SetIdentity();
	if (m_node->GetNodeAttribute())
	{
		const FbxVector4 lT = m_node->GetGeometricTranslation(FbxNode::eSourcePivot);
		const FbxVector4 lR = m_node->GetGeometricRotation(FbxNode::eSourcePivot);
		const FbxVector4 lS = m_node->GetGeometricScaling(FbxNode::eSourcePivot);
		matrixGeo.SetT(lT);
		matrixGeo.SetR(lR);
		matrixGeo.SetS(lS);
	}
	return matrixGeo;
}

FBXMatrix4 FBXNode::GeometricTransform::get()
{
	return FBXMatrix4(getGeometricTransform());
}

FBXMatrix4 FBXNode::LocalTransformTrue::get()
{
	return FBXMatrix4(m_node->EvaluateLocalTransform() * getGeometricTransform());
}

FBXMatrix4 FBXNode::GlobalTransformTrue::get()
{
	return FBXMatrix4(m_node->EvaluateGlobalTransform() * getGeometricTransform());
}

FBXMatrix4 FBXNode::GlobalTransform::get()
{
	return FBXMatrix4(m_node->EvaluateGlobalTransform());
}

FBXMatrix4 FBXNode::LocalTransform::get()
{
	return FBXMatrix4(m_node->EvaluateLocalTransform());
}


FBXMesh ^FBXNode::Mesh::get()
{
	return gcnew FbxWrapper::FBXMesh(m_node->GetMesh());
}