#pragma once

#include "Polygon.h"
#include "LayerElementTool.h"

using namespace System::Collections::Generic;

namespace FbxWrapper
{
	public ref class FBXMesh
	{
	private:
		FbxMesh * m_mesh;
	internal:
		FBXMesh(FbxMesh* pmesh);

	public:
		/// <summary>
		/// Get how the layer element (normals, texture, etc...) are stored.
		/// http://docs.autodesk.com/FBX/2014/ENU/FBX-SDK-Documentation/index.html?url=cpp_ref/_normals_2main_8cxx-example.html,topicNumber=cpp_ref__normals_2main_8cxx_example_html2cff27fd-bb1a-4142-982e-65fd5d6ad951
		/// If ByControlPoint the n'th element match with n'th control point
		/// </summary>
		MappingMode GetMappingMode(LayerElementType layer);
		/// <summary>
		/// </summary>
		//void SetMappingMode(LayerElementType elementType, MappingMode mode);

		/// <summary>
		/// Gets and sets the control points size, if size is different the items are not cleaned !
		/// </summary>
		property_rw(int, ControlPointsCount);
		/// <summary>
		/// sets the vertices, if index is out of limit, the array is resize internaly but function return if success 
		/// </summary>
		bool SetControlPointAt(int index, double x, double y, double z);
		/// <summary>
		/// get the vertices, if index is out of range, zero vector is returned
		/// </summary>
		FBXVector3 FBXMesh::GetControlPointAt(int index);
		
		property_rw(array<FBXVector3>^, ControlPoints);
		
		/// <summary>
		/// Gets the normal from layer 0
		/// </summary>
		array<FBXVector3>^ GetNormals();
		/// <summary>
		/// Gets the tangents from layer 0
		/// </summary>
		array<FBXVector4>^ GetTangents();
		/// <summary>
		/// Gets the uv from layer 0
		/// </summary>
		array<FBXVector2>^ GetTextCoords();
		/// <summary>
		/// Gets the colors from layer 0
		/// </summary>
		array<FBXColour>^ GetVertexColours();
		/// <summary>
		/// Set element in layer 0. A new layer element will be create, the old element are deleted
		/// </summary>
		bool SetNormals(array<FBXVector3>^ normals, MappingMode mapping, ReferenceMode referencing);
		/// <summary>
		/// Set element in layer 0. A new layer element will be create, the old element are deleted
		/// </summary>
		bool SetTangents(array<FBXVector4>^ tangents, MappingMode mapping, ReferenceMode referencing);
		/// <summary>
		/// Set element in layer 0. A new layer element will be create, the old element are deleted
		/// </summary>
		bool SetTextCoords(array<FBXVector2>^ textcoords, MappingMode mapping, ReferenceMode referencing);
		/// <summary>
		/// Set element in layer 0. A new layer element will be create, the old element are deleted
		/// </summary>
		bool SetVertexColours(array<FBXColour>^ colours, MappingMode mapping, ReferenceMode referencing);

		/// <summary>
		/// Get all polygons
		/// </summary>
		array<Polygon>^ GetPolygons();
		/// <summary>
		/// Add a new polygon
		/// </summary>
		void AddPolygon(array<int> ^indices);
		

		property_r(bool, Triangulated);
		
		property int UVLayer;

	};
}
