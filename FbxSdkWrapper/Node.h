#pragma once

#include "Manager.h"
#include "NodeAttribute.h"
#include "Mesh.h"


namespace FbxWrapper
{
	ref class FBXManager;

	/// <summary>
	/// Basic scene's node hierarchy.
	/// Represents an element in the scene graph. A scene graph is a tree of FbxNode objects.
	/// </summary> 
	public ref class FBXNode
	{

	public:
		//property string^ Name {string^ get(); }
		property_r(string^, Name);

		/// <summary>
		/// For root node this member is NULL
		/// </summary>
		property_r(NodeAttribute^, FBXNode::Attribute);

		/// <summary>
		/// Gets and sets the position of this node.
		/// The value is an offset from the corresponding default TRS property vector for the parent node
		/// </summary>
		property_rw(FBXVector3, Position);

		/// <summary>
		/// Gets and sets the rotation of this node.
		/// The value is an offset from the corresponding default TRS property vector for the parent node
		/// </summary>
		property_rw(FBXVector3, Rotation);

		/// <summary>
		/// Gets and sets the scale of this node.
		/// The value is an offset from the corresponding default TRS property vector for the parent node
		/// </summary>
		property_rw(FBXVector3, Scale);

		/// <summary>
		/// Gets the first mesh attribute of this node if it exists, otherwise null.
		// (node attribute casted to a FbxMesh pointer)
		/// </summary>
		property_r(FbxWrapper::FBXMesh^, Mesh);

		/// <summary>
		/// Get the node's default global transformation matrix. (default TRS properties)
		/// A node's global transformation matrix of cannot be explicitly set in the FBX SDK.
		/// </summary>
		property_r(FBXMatrix4, GlobalTransform);
		/// <summary>
		/// Get the node's default local transformation matrix. (default TRS properties)
		/// The node�s default TRS properties are therefore local to the parent node.
		/// </summary>
		property_r(FBXMatrix4, LocalTransform);
		/// <summary>
		/// Get the attribute's default local transformation matrix, but it's not clear how it works
		/// https://help.autodesk.com/view/FBX/2017/ENU/?guid=__files_GUID_C35D98CB_5148_4B46_82D1_51077D8970EE_htm
		/// </summary>
		property_r(FBXMatrix4, GeometricTransform);
		/// <summary>
		/// Get the node's default local transformation matrix. (default TRS properties)
		/// The node�s default TRS properties are therefore local to the parent node.
		/// </summary>
		property_r(FBXMatrix4, LocalTransformTrue);
		/// <summary>
		/// Get the node's default local transformation matrix. (default TRS properties)
		/// The node�s default TRS properties are therefore local to the parent node.
		/// </summary>
		property_r(FBXMatrix4, GlobalTransformTrue);


		int GetChildCount();
		FBXNode ^GetChild(int index);
		void AddChild(FBXNode ^node);

	internal:
		FbxNode* m_node;
		FBXNode(FbxNode* pnode);
		FBXNode(FBXManager^ manager, string^ name);
		FBXNode(FBXManager^ manager, string^ name, AttributeType etype);

		FbxAMatrix getGeometricTransform();
	};
}
