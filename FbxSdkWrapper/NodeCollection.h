#pragma once

#include "Node.h"

using namespace System::Collections;

namespace FbxWrapper
{
	generic<class Node> public ref class NodeCollection : Generic::IList<Node>
	{
	private:
		int items;
		array<Node>^ collection;

	public:
		
		virtual property_r(bool, IsReadOnly);
		virtual property_r(int, Count);
		//virtual property_r(Node, default);
		virtual bool Contains(Node node);
		virtual bool Remove(Node node);
		virtual int IndexOf(Node node);
		virtual void Add(Node node);
		virtual void Clear();
		virtual void CopyTo(array<Node, 1>^ destination, int length);
		virtual void Insert(int index, Node node);
		virtual void RemoveAt(int index);

		virtual System::Collections::IEnumerator^ GetEnumerator2()
		{
			return nullptr;
		}
		virtual Generic::IEnumerator<Node>^ GetEnumerator()
		{
			return nullptr;
		}
	};
}
