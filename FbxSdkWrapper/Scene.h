#pragma once

#include "Properties.h"
#include "Manager.h"
#include "Node.h"


namespace FbxWrapper
{
	//FUCK FORWARD DECLARATION ERROR WHEN COMPILING ??? this is one of the many reasons why I hate C++
	ref class FBXManager;
	ref class FBXNode;

	/// <summary>
	/// Represents an FBX scene.
	/// </summary>
	public ref class FBXScene
	{
	public:
		/// <summary>
		/// Initialize a new scene with its name, root node are created automatically
		/// </summary> 
		FBXScene(FBXManager^ manager, string ^name);
		/// <summary>
		/// Import a supported fbx file defined by fileformatID (-1 if autodetect) found with
		/// Manager.GetSupportedReaders list
		/// </summary>
	    static FBXScene ^Import(FBXManager^ manager, string ^filepath, int fileformat);
		/// <summary>
		/// Export a supported fbx file defined by fileformatID (-1 for native writer... i don't known that it's)
		/// found with Manager.GetSupportedWriters list
		/// </summary>
		void Export(FBXScene ^scene, string ^filepath, int fileFormat);
		
		property_r(FBXNode^, RootNode);

		// file version from importer, will be stored to remember when exporting
		property Version^ FileVersion;
	
	private:
		FbxScene* m_scene;
		FBXNode^ m_rootNode;
		FBXManager^ manager;
	};
}

